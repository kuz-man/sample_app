class UserMailer < ApplicationMailer

  def account_activation(user)
    @user = user
    mail to: user.email, subject: "ISN account activation"
  end

  def password_reset(user)
    @user = user
    mail to: user.email, subject: "ISN Password reset"
  end
end
