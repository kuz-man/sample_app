class MicropostsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user, only: :destroy
  
  def create
    @feed_items = current_user.feed.paginate(page: params[:page])
    @micropost = current_user.microposts.build(micropost_params)
    if @micropost.save!
      # flash[:success] = "Micropost created!"
      respond_to do |format|
        format.html { redirect_to root_path }
        format.js
      end
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end

  def polling
    @micropost = current_user.microposts.build
    @feed_items = current_user.feed.paginate(page: params[:page])
    @timestamp = Time.now - Micropost.first.created_at < 16.seconds
  end
  
  # def create
  #   @micropost  = current_user.microposts.build(params[:micropost])
  #   if 
  #     @micropost.save!
  #     respond_to do |format|
  #       format.html { redirect_to root_path }
  #       format.js
  #     end
  #   end
  # end

  
  def destroy
    @micropost.destroy
    @feed_items = current_user.feed.paginate(page: params[:page])
    # flash[:success] = "Micropost deleted!"
    respond_to do |format|
      format.html { redirect_to request.referrer || root_url }
      format.js
    end
  end
  
  private
  
    def micropost_params
      params.require(:micropost).permit(:content, :picture)
    end
    
    def correct_user
      @micropost = current_user.microposts.find_by(id: params[:id])
      redirect_to root_url if @micropost.nil?
    end
end
