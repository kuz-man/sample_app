class CheckinsController < ApplicationController
  before_action :logged_in_user, only: [:new, :create, :edit, :update]
  
  def new
    @checkin = current_user.build_checkin(checkin_params)
    if @checkin.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def create
    @checkin = current_user.build_checkin(lat: params['lat'], lng: params['lng'])
    respond_to do |format|
      format.json do
        if @checkin.save!
          render :json => { :message => "Success" }
        end
      end
    end
  end
  
  def edit
    @checkin = current_user.build_checkin(checkin_params)
    if @checkin.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def update
    @checkin = current_user.build_checkin(checkin_params)
    if @checkin.save
      flash[:success] = "Micropost created!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  
  def set_location
    respond_to do |format|
      format.json
    end
    @checkin = current_user.build_checkin(params['checkin_location'])
    if @checkin.save
      flash[:success] = "You just checked in!"
      redirect_to root_url
    else
      @feed_items = []
      render 'static_pages/home'
    end
  end
  helper_method :get_location
  
  private
  
    def checkin_params
      params.require(:checkin).permit(:user, :lat, :lng)
    end
end
