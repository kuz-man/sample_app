class StaticPagesController < ApplicationController
  def home
    if logged_in?
      @micropost = current_user.microposts.build
      @feed_items = current_user.feed.paginate(page: params[:page])
      @gmaps_users = current_user.gmaps_feed
      @hash = Gmaps4rails.build_markers(@gmaps_users) do |spot, marker|
        marker.lat spot.lat
        marker.lng spot.lng
        marker.infowindow spot.user.name
      end
      # @user_coords = Gmaps4rails.build_markers(current_user) do |user, marker|
      #   marker.lat current_user.checkin.lat
      #   marker.lng current_user.checkin.lng
      # end
      if !current_user.checkin.nil?
        @user_lat = current_user.checkin.lat
        @user_lng = current_user.checkin.lng
      end
    end
  end

  def help
  end
  
  def about
  end
  
  def contact
  end
end
