class Checkin < ActiveRecord::Base
  belongs_to :user
  validates :user_id, presence: true
  validates :lat, presence: true
  validates :lng, presence: true
end
