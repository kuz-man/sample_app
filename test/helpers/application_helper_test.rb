require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,          "Intergalactic Social Network"
    assert_equal full_title("Help"),  "Help | Intergalactic Social Network"
  end
end