class CreateCheckins < ActiveRecord::Migration
  def change
    create_table :checkins do |t|
      t.decimal :lat
      t.decimal :lng
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
